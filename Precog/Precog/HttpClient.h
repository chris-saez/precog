//
//  HttpClient.h
//  Precog
//
//  Created by Christopher Saez on 28/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Prediction.h"

typedef void (^SuccessBlock) (NSArray<Prediction*> *predictions);

@interface HttpClient : NSObject
+(void) askForPredictions1week:(SuccessBlock) success;
+(void) askForPredictions2week:(SuccessBlock) success;
+(void) sendPush:(NSInteger) numberOfPersons date:(NSDate*) date;
+(void) sendPushTom;
@end
