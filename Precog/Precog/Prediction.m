//
//  Prediction.m
//  Precog
//
//  Created by Christopher Saez on 28/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "Prediction.h"
#import <CoreLocation/CLLocation.h>

@implementation Prediction


+(Prediction*) getPrediction:(NSDictionary*) predictionDict{

	Prediction *prediction = [Prediction new];
	
	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	prediction.date = [formatter dateFromString:[predictionDict objectForKey:@"datetime"]];
	prediction.type = [[predictionDict objectForKey:@"type"] integerValue];
	prediction.absences = [[predictionDict objectForKey:@"absence"] integerValue];
	prediction.fiabilite = [[predictionDict objectForKey:@"fiabilite"] integerValue];
	prediction.fiabilite = [[predictionDict objectForKey:@"fiabilite"] integerValue];
	
	
	return prediction;
}



@end
