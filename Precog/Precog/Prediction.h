//
//  Prediction.h
//  Precog
//
//  Created by Christopher Saez on 28/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PredictionType) {
	PredictionTypeMaladie,
	PredictionTypeConge,
	PredictionTypeInterne,
	PredictionTypeDivers
};

@interface Prediction : NSObject

@property (strong, nonatomic) NSDate *date;
@property (nonatomic, assign) PredictionType type;
@property (nonatomic, assign) NSInteger absences;
@property (nonatomic, assign) NSInteger fiabilite;

 
+(Prediction*) getPrediction:(NSDictionary*) predictionDict;

@end
