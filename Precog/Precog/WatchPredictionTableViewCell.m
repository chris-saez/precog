//
//  WatchPredictionTableViewCell.m
//  Precog
//
//  Created by Christopher Saez on 29/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "WatchPredictionTableViewCell.h"

@interface WatchPredictionTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *alertLabel;

@end

@implementation WatchPredictionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	
	}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCell{

	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setDateFormat:@"HH'H'mm"];
	
	self.alertLabel.text = [NSString stringWithFormat:@"Mvt. à risque détecté - %@", [formatter stringFromDate:[NSDate date]]];

}


@end
