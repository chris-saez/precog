//
//  WatchPredictionTableViewCell.h
//  Precog
//
//  Created by Christopher Saez on 29/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"

@interface WatchPredictionTableViewCell : MGSwipeTableCell

@property (nonatomic, strong) NSIndexPath *indexPath;

-(void) configureCell;

@end
