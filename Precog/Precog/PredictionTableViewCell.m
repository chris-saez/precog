//
//  PredictionTableViewCell.m
//  Precog
//
//  Created by Christopher Saez on 28/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "PredictionTableViewCell.h"

@interface PredictionTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *cicleView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *effectifReason;
@property (weak, nonatomic) IBOutlet UIImageView *pictoColor;
@property (weak, nonatomic) IBOutlet UIView *fiabiliteContainerView;
@property (weak, nonatomic) IBOutlet UILabel *fiabilityPercentLabel;
@property (weak, nonatomic) IBOutlet UILabel *fiabiliteLabel;
@property (weak, nonatomic) IBOutlet UILabel *absenceLabel;

@end

@implementation PredictionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCell:(Prediction*) prediction{
	self.cicleView.layer.cornerRadius = 25;
	
	self.pictoColor.image = [UIImage imageNamed:[NSString stringWithFormat:@"notif%ld", (long)prediction.type]];
	self.fiabiliteContainerView.alpha = prediction.fiabilite / 100.f;
	if (prediction.fiabilite < 50){
		self.fiabiliteLabel.textColor = [UIColor blackColor];
		self.fiabilityPercentLabel.textColor = [UIColor blackColor];
	} else {
		self.fiabiliteLabel.textColor = [UIColor whiteColor];
		self.fiabilityPercentLabel.textColor = [UIColor whiteColor];
	}
	self.fiabilityPercentLabel.text = [NSString stringWithFormat:@"%ld%%", (long)prediction.fiabilite];
	if (prediction.type == PredictionTypeMaladie){
		self.effectifReason.text = @"ABSENCE MALADIE";
	} else if (prediction.type == PredictionTypeConge){
		self.effectifReason.text = @"ABSENCE CONGÉ";
	} else if (prediction.type == PredictionTypeInterne){
		self.effectifReason.text = @"ABSENCE INTERNE";
	} else {
		self.effectifReason.text = @"ABSENCE DIVERSE";
	}
	
	self.absenceLabel.text = [NSString stringWithFormat:@"%ld",(long)prediction.absences];
	
	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setDateFormat:@"dd MMM yyyy"];
	self.dateLabel.text = [[formatter stringFromDate:prediction.date] uppercaseString];
	
}

@end
