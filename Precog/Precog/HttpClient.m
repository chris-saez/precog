//
//  HttpClient.m
//  Precog
//
//  Created by Christopher Saez on 28/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "HttpClient.h"

#import <AFNetworking.h>
#define randstadToken @"8a63915f94f9c3453c51e4c707b96104c4580217d4a9dcf6c809a341e5156597"

#define operationelToken @"db32df7bfbd6477269bc661e02bf886bf19efc667e71c37be08a202a2e1b8854"

@implementation HttpClient

+(void) askForPredictions1week:(SuccessBlock) success{
	NSURL *url = [NSURL URLWithString:@"https://hackaton-push.precog.link-value.fr:9090/prediction"];
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	
	
	
	NSDictionary *params = @{@"msg_id": @"724b88ba-d995-46b1-bf8c-2c1914124e9c",
							 @"_text": @"donne moi les predictions pour la semaine prochaine",
							 @"entities": @{
									 @"datetime": @[
            @{
				@"confidence": @0.9983461361006392,
				@"values": @[
						@{
							@"value": @"2017-01-30T00:00:00.000-08:00",
							@"grain": @"week",
							@"type": @"value"
							}
						],
				@"value": @"2017-01-30T00:00:00.000-08:00",
				@"grain": @"week",
				@"type": @"value"
				}
			]}};
	
	[manager POST:url.absoluteString parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		
		NSArray *array =  [responseObject objectForKey:@"predictions"];
		NSMutableArray<Prediction*> *predictions = [NSMutableArray new];
		for (NSDictionary *dict in array){
			Prediction *prediction = [Prediction getPrediction:dict];
			[predictions addObject:prediction];
		}
		
		success(predictions);
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		
	}];
}

+(void) askForPredictions2week:(SuccessBlock) success{
	NSURL *url = [NSURL URLWithString:@"https://hackaton-push.precog.link-value.fr:9090/prediction"];
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	manager.responseSerializer = [AFJSONResponseSerializer serializer];
	
	
	
	NSDictionary *params = @{
							 @"msg_id": @"724b88ba-d995-46b1-bf8c-2c1914124e9c",
							 @"_text": @"donne moi les predictions pour la semaine prochaine",
							 @"entities": @{
									 @"datetime": @[
            @{
				@"confidence": @0.9983461361006392,
				@"values": @[
						@{
							@"value": @"2017-01-30T00:00:00.000-08:00",
							@"grain": @"week",
							@"type": @"value"
							}
						],
				@"value": @"2017-02-06T00:00:00.000-08:00",
				@"grain": @"week",
				@"type": @"value"
				}
			]
									 }
							 };
	
	[manager POST:url.absoluteString parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		NSArray *array = [responseObject objectForKey:@"predictions"];
		NSMutableArray<Prediction*> *predictions = [NSMutableArray new];
		for (NSDictionary *dict in array){
			Prediction *prediction = [Prediction getPrediction:dict];
			[predictions addObject:prediction];
		}
		
		success(predictions);
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
	}];
}

+(void) sendPush:(NSInteger) numberOfPersons date:(NSDate*) date{

	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setDateFormat:@"EEEE dd MMM yyyy"];
	NSString *dateString = [formatter stringFromDate:date];

	NSString* sentence = [NSString stringWithFormat:@"L'usine PSA recherche %ld operateur(s) en Interim pour le %@", (long)numberOfPersons, dateString];
	
	[formatter setDateFormat:@"dd.MM.yy - 9'h'00"];
	NSString *pushString = [formatter stringFromDate:date];
	
	NSURL *url = [NSURL URLWithString:@"https://hackaton-push.precog.link-value.fr/apns"];
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	NSDictionary *dict = @{@"tokens": @[randstadToken], @"alert": sentence, @"data": @{@"date": pushString, @"number": @(numberOfPersons)}, @"sound": @"WTF", @"badge": @1};
	[manager POST:url.absoluteString parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		//
	} ];
	
}

+(void) sendPushTom{
	NSURL *url = [NSURL URLWithString:@"https://hackaton-push.precog.link-value.fr/apns"];
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
	manager.requestSerializer = [AFJSONRequestSerializer serializer];
	NSDictionary *dict = @{@"tokens": @[operationelToken], @"alert": @"Tom, j'ai détecté une anomalie... J'arrive !",@"data": @{@"date": @"ggg", @"number": @1}, @"sound": @"WTF", @"badge": @1};
	[manager POST:url.absoluteString parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
		
	} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
		//
	} ];
}

@end
