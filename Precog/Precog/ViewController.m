//
//  ViewController.m
//  Precog
//
//  Created by Christopher Saez on 26/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "ViewController.h"

#import <SVProgressHUD.h>
#import "HttpClient.h"
#import "Prediction.h"
#import "PredictionTableViewCell.h"
#import "MGSwipeTableCell.h"
#import "WatchPredictionTableViewCell.h"
#import <AVFoundation/AVFoundation.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *microButton;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UILabel *alertNumber;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;

@property (nonatomic, strong) NSArray<Prediction*> *predictions;
@property (nonatomic, strong) NSMutableArray *watchPredictions;

@property (nonatomic, assign) int inc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *welcomeConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.inc = 0;
	[self.navigationController.navigationBar setHidden:YES];
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	
	self.alertNumber.layer.cornerRadius = 6;
	self.alertNumber.alpha = 0;
	
	NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"];
	NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
	[self.webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
 
	UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"”Wisdom”  Would like to Access the Microphone" message:@"Pour utiliser la reconnaissance vocale de Wisdom, nous avons besoin d'accéder au micro" preferredStyle:(UIAlertControllerStyleAlert)];
	
	UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
														  handler:^(UIAlertAction * action) {
														  
															  self.welcomeConstraint.constant = 388;
															  [UIView animateWithDuration:0.4 animations:^{
																  [self.view layoutIfNeeded];
															  }];
														  }];
	
	UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Don't allow" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) {
														 
															 self.welcomeConstraint.constant = 388;
															 [UIView animateWithDuration:0.5 animations:^{
																 [self.view layoutIfNeeded];
															 }];
														 }];
	
	[vc addAction:cancelAction];
	[vc addAction:defaultAction];
	
	[self presentViewController:vc animated:YES completion:nil];
	
	
	self.tableview.delegate = self;
	self.tableview.dataSource = self;
	
	UINib *nib = [UINib nibWithNibName:@"PredictionTableViewCell" bundle:nil];
	[self.tableview registerNib:nib forCellReuseIdentifier:@"PredictionTableViewCell"];
	
	UINib* nib2 = [UINib nibWithNibName:@"WatchPredictionTableViewCell" bundle:nil];
	[self.tableview registerNib:nib2 forCellReuseIdentifier:@"WatchPredictionTableViewCell"];
	
	
	
	
}

- (IBAction)didTapStopButton:(id)sender {
	AudioServicesPlaySystemSound (1004);
	self.welcomeConstraint.constant = -20;
	[UIView animateWithDuration:0.4 animations:^{
		[self.view layoutIfNeeded];
	}];

	[SVProgressHUD show];
	if (self.inc == 0){
		self.inc++;
		[HttpClient askForPredictions1week:^(NSArray<Prediction*> *predictions){
			[SVProgressHUD dismiss];
			self.bottomConstraint.constant = -76;
			[UIView animateWithDuration:0.3 animations:^{
				[self.view layoutIfNeeded];
				
			} completion:^(BOOL finished) {
				[UIView animateWithDuration:0.3 animations:^{
					self.webview.alpha = 0;
				} completion:^(BOOL finished) {
					self.predictions = predictions;
					[self.tableview reloadData];
					CATransition *animation = [CATransition animation];
					[animation setDelegate:self];
					[animation setDuration:1.0f];
					[animation setTimingFunction:UIViewAnimationCurveEaseInOut];
					[animation setType:@"rippleEffect"];
					[self.tableview.layer addAnimation:animation forKey:NULL];
				}];
			}];
		}];
	} else {
		[HttpClient askForPredictions2week:^(NSArray<Prediction*> *predictions){
			[SVProgressHUD dismiss];
			self.bottomConstraint.constant = -76;
			[UIView animateWithDuration:0.3 animations:^{
				[self.view layoutIfNeeded];
				
			} completion:^(BOOL finished) {
				[UIView animateWithDuration:0.3 animations:^{
					self.webview.alpha = 0;
				} completion:^(BOOL finished) {
					self.predictions = predictions;
					[self.tableview reloadData];
					CATransition *animation = [CATransition animation];
					[animation setDelegate:self];
					[animation setDuration:1.0f];
					[animation setTimingFunction:UIViewAnimationCurveEaseInOut];
					[animation setType:@"rippleEffect"];
					[self.tableview.layer addAnimation:animation forKey:NULL];
				}];
			}];
		}];
	}
	
}

- (IBAction)didTapMicro:(id)sender {
	[UIView animateWithDuration:0.3 animations:^{
		self.webview.alpha = 1;
		
	} completion:^(BOOL finished) {
		self.bottomConstraint.constant = 23;
		[UIView animateWithDuration:0.3 animations:^{
			[self.view layoutIfNeeded];
			
		} completion:^(BOOL finished) {
			//
		}];
	}];
}



#pragma mark - tableview

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (section == 0){
		return self.watchPredictions.count;
	} else {
		return self.predictions.count;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (indexPath.section == 1){
		return 90;
	} else {
		return 65;
	}
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	if (indexPath.section == 1){
		PredictionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PredictionTableViewCell" forIndexPath:indexPath];
		cell.indexPath = indexPath;
		[cell configureCell:self.predictions[indexPath.row]];
		cell.delegate = self;
		//configure right buttons
		cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Notifier" backgroundColor:UIColorFromRGB(0xefa415)]];
		cell.rightSwipeSettings.transition = MGSwipeTransitionBorder;
		return cell;
	} else {
		WatchPredictionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WatchPredictionTableViewCell" forIndexPath:indexPath];
		[cell configureCell];
		cell.delegate = self;
		cell.indexPath = indexPath;
		cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Contacter" backgroundColor:UIColorFromRGB(0xefa415)]];
		cell.rightSwipeSettings.transition = MGSwipeTransitionBorder;
		return cell;
	}
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	if (section == 1 && self.predictions.count){
		UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 17)];
		
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, 100, 15)];
		label.text = @"ALERTES";
		label.font = [UIFont systemFontOfSize:13];
		label.textColor = [UIColor grayColor];
		[label sizeToFit];
		[view addSubview:label];
		
		UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(300, 0, 100, 15)];
		label2.text = @"ABSENTS";
		label2.font = [UIFont systemFontOfSize:13];
		label2.textColor = [UIColor grayColor];
		[label2 sizeToFit];
		[view addSubview:label2];
		
		return view;
	}
	else
		return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	if (section == 1){
		return 17;
	} else {
		return 0.01;
	}
}
-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion{
	
	if ([cell isKindOfClass:[PredictionTableViewCell class]]){
		
		PredictionTableViewCell *pcell = (PredictionTableViewCell*)cell;
		Prediction *prediction = self.predictions[pcell.indexPath.row];
		[HttpClient sendPush:prediction.absences date:prediction.date];
	} else {
		
		[HttpClient sendPushTom];
	}
	return YES;
}

-(void) notifyWatch{
	NSObject *object = [NSObject new];
	self.watchPredictions = [NSMutableArray new];
	[self.watchPredictions addObject:object];
	[self.tableview reloadData];
	[self.tableview scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
	AudioServicesPlaySystemSound (1016);
	
	
	[self performSelector:@selector(animate) withObject:nil afterDelay:0.4];
	
}


-(void) animate{
	CATransition *animation = [CATransition animation];
	[animation setDelegate:self];
	[animation setDuration:1.0f];
	[animation setTimingFunction:UIViewAnimationCurveEaseInOut];
	[animation setType:@"rippleEffect"];
	UITableViewCell *cell = [self.tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] ];
	[cell.contentView.layer addAnimation:animation forKey:NULL];
}

@end
