//
//  PredictionTableViewCell.h
//  Precog
//
//  Created by Christopher Saez on 28/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prediction.h"
#import "MGSwipeTableCell.h"

@interface PredictionTableViewCell : MGSwipeTableCell


-(void) configureCell:(Prediction*) prediction;

@property (nonatomic, strong) NSIndexPath *indexPath;

@end
