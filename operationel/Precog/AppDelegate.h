//
//  AppDelegate.h
//  Precog
//
//  Created by Christopher Saez on 26/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

