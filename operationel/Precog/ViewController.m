//
//  ViewController.m
//  Precog
//
//  Created by Christopher Saez on 26/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NSNumber *inc;
@property (weak, nonatomic) IBOutlet UIButton *kitButton;

@end

@implementation ViewController

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	[self.navigationController.navigationBar setHidden:YES];
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	

    // Do any additional setup after loading the view, typically from a nib.
	
	
	
}

-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[NSTimer scheduledTimerWithTimeInterval:0.022
									 target:self
								   selector:@selector(handleTimer:)
								   userInfo:nil
									repeats:YES];
}

-(void) handleTimer:(NSTimer *)timer{
	if ([self.inc integerValue] < 100){
	self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", [self.inc intValue]]];
	}
	NSInteger v = [self.inc integerValue];
	v++;
	if (v >= 100){
		[timer invalidate];
		[UIView animateWithDuration:0.3 animations:^{
			self.kitButton.alpha = 1;
		}];
	
	}
	self.inc = [NSNumber numberWithInteger:v];

}
- (IBAction)didTapKit:(id)sender {
	
	UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"Adhérez au programme ResearchKit?" message:@"ResearchKit est une initiative visant à récolter anonyment les données santé de l'AppleWatch dans le but de la prévention de maladie grâce aux données récoltées dans le contexte personnelle et professionnelle." preferredStyle:(UIAlertControllerStyleAlert)];
	
	UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Adhérer" style:UIAlertActionStyleDestructive
														  handler:^(UIAlertAction * action) {}];
	
	UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Plus tard" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) {}];
	
	[vc addAction:cancelAction];
	[vc addAction:defaultAction];
	
	[self presentViewController:vc animated:YES completion:nil];
	
}

-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}



@end
