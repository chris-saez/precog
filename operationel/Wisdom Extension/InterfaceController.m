//
//  InterfaceController.m
//  Wisdom Extension
//
//  Created by Christopher Saez on 29/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "InterfaceController.h"
#import <CoreMotion/CoreMotion.h>

@interface InterfaceController()

@property (nonatomic, strong) CMMotionManager *motionMgr;
@property (nonatomic, strong) NSOperationQueue *motionQueue;

@end


@implementation InterfaceController

- (void)awakeWithContext:(id)context {
	[super awakeWithContext:context];
	CMMotionManager *motionMgr = [[CMMotionManager alloc] init];
	self.motionQueue = [NSOperationQueue mainQueue];
	self.motionMgr = motionMgr;
	
	// Configure interface objects here.
}

- (void)willActivate {
	// This method is called when watch view controller is about to be visible to user
	[super willActivate];
	
	
	[self.motionMgr setAccelerometerUpdateInterval:0.2];
	[self.motionMgr startAccelerometerUpdatesToQueue:self.motionQueue withHandler:^(CMAccelerometerData *accel, NSError *error){
		CMAcceleration a = accel.acceleration;
		
		NSDate *date  = [NSDate date];
		
		
		if (abs(a.x) + abs(a.y) + abs(a.z) > 3){
			NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
											initWithURL:[NSURL
														 URLWithString:@"https://hackaton-push.precog.link-value.fr:9090/accelero"]];
			NSDateFormatter *formatter = [NSDateFormatter new];
			[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
			NSString *dateString = [formatter stringFromDate:date];
			NSDictionary *dict = @{@"x": @(a.x), @"y": @(a.y), @"z": @(a.z), @"datetime": dateString};
			[request setHTTPMethod:@"POST"];
			NSError *error2;
			NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error2];
			[request setHTTPBody:postData];
			[request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
			NSURLSession *session = [NSURLSession sharedSession];
			NSURLSessionDataTask *task = [session dataTaskWithRequest:request
													completionHandler:
										  ^(NSData *data, NSURLResponse *response, NSError *error) {
											  // ...
										  }];
			
			[task resume];
		}
		
		
	}];
	
}
- (IBAction)test {
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
									initWithURL:[NSURL
												 URLWithString:@"https://hackaton-push.precog.link-value.fr:9090/accelero"]];
	NSDateFormatter *formatter = [NSDateFormatter new];
	[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
	NSString *dateString = [formatter stringFromDate:[NSDate date]];
	NSDictionary *dict = @{@"x": @(-3), @"y": @(3), @"z": @(-3), @"datetime": dateString};
	[request setHTTPMethod:@"POST"];
	NSError *error2;
	NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error2];
	[request setHTTPBody:postData];
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
	NSURLSession *session = [NSURLSession sharedSession];
	NSURLSessionDataTask *task = [session dataTaskWithRequest:request
											completionHandler:
								  ^(NSData *data, NSURLResponse *response, NSError *error) {
									  // ...
								  }];
	
	[task resume];
}

- (void)didDeactivate {
	// This method is called when watch view controller is no longer visible
	[super didDeactivate];
}

@end



