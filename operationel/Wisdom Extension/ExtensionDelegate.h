//
//  ExtensionDelegate.h
//  Wisdom Extension
//
//  Created by Christopher Saez on 29/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate>

@end
