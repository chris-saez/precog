//
//  ViewController.m
//  Precog
//
//  Created by Christopher Saez on 26/01/2017.
//  Copyright © 2017 Christopher Saez. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *numberOperateurLabel;
@property (weak, nonatomic) IBOutlet UILabel *date;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	[self.navigationController.navigationBar setHidden:YES];
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	

    // Do any additional setup after loading the view, typically from a nib.
}


-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

-(void) configure:(NSString*) date number:(NSInteger) number{
	if (number > 1 ){
		self.numberOperateurLabel.text = [NSString stringWithFormat:@"%d Opérationels",number];
	} else {
		self.numberOperateurLabel.text = @"1 Opérationel";
	}
	self.date.text = date;
}


@end
